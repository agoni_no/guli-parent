package com.xunqi.commonutils.constant;

/**
 * @Description: 课程常量
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-04 09:35
 **/
public class CourseConstant {

    /* 未发布 */
    public static final String DRAFT = "Draft";

    /* 已发布 */
    public static final String NORMAL = "Normal";

}
