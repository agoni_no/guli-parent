package com.xunqi.eduservice.controller.front;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduCourse;
import com.xunqi.eduservice.entity.EduTeacher;
import com.xunqi.eduservice.service.EduCourseService;
import com.xunqi.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-11 08:56
 **/

@RestController
@RequestMapping(value = "/eduservice/teacherfront")
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    /**
     * 分页查询讲师的方法
     * @param page
     * @param limit
     * @return
     */
    @PostMapping(value = "/getTeacherFrontList/{page}/{limit}")
    public R getTeacherFrontList(@PathVariable("page") Integer page,
                                 @PathVariable("limit") Integer limit) {

        Page<EduTeacher> eduTeacherPage = new Page<>(page,limit);
        //返回分页中的所有数据
        Map<String,Object> map =  teacherService.getTeacherFrontList(eduTeacherPage);

        return R.ok().data(map);
    }


    /**
     * 根据id查询讲师信息
     * @param teacherId
     * @return
     */
    @GetMapping(value = "/getTeacherFrontInfo/{teacherId}")
    public R getTeacherFrontInfo(@PathVariable("teacherId") String teacherId) {

        //1、根据讲师id查询讲师基本信息
        EduTeacher eduTeacher = teacherService.getById(teacherId);

        //2、根据讲师id查询所讲课程
        List<EduCourse> eduCourses =  courseService.getTeacherIdByInfo(teacherId);

        return R.ok().data("eduTeacher",eduTeacher).data("eduCourses",eduCourses);
    }

}
