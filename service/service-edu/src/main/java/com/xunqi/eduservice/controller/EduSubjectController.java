package com.xunqi.eduservice.controller;

import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduSubject;
import com.xunqi.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


/**
 * 课程科目
 * @author 夏沫止水
 * @email HeJieLin@gulimall.com
 * @date 2020-07-29 15:22:28
 */
@Api(value = "课程分类管理",tags = "课程分类管理")
@RestController
@RequestMapping("/eduservice/subject")
public class EduSubjectController {

    @Autowired
    private EduSubjectService eduSubjectService;

    //添加课程分类
    //获取上传过来文件，把文件内容读取出来

    @ApiOperation(value = "添加课程科目信息",notes = "添加课程科目信息",httpMethod = "POST")
    @PostMapping(value = "addSubject")
    public R addSubject(MultipartFile file) {

        //上传过来的Excel文件
        eduSubjectService.saveSubject(file,eduSubjectService);

        return R.ok();
    }

    //课程分类列表(树形菜单)
    @ApiOperation(value = "课程分类列表",notes = "课程分类列表",httpMethod = "GET")
    @GetMapping(value = "/getAllSubject")
    public R getAllSubject() {
        List<EduSubject> twoSubjectList = eduSubjectService.getAllOneTwoSubject();

        return R.ok().data("twoSubjectList",twoSubjectList);
    }

}
