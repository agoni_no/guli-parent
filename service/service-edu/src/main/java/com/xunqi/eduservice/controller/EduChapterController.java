package com.xunqi.eduservice.controller;


import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduChapter;
import com.xunqi.eduservice.entity.chapter.ChapterVo;
import com.xunqi.eduservice.service.EduChapterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
@Api(value = "课程大纲列表管理",tags = "课程大纲列表管理")
@RestController
@RequestMapping("/eduservice/chapter")
public class EduChapterController {

    @Autowired
    private EduChapterService eduChapterService;

    /**
     * 课程大纲列表，根据课程id进行查询
     * @return
     */
    @ApiOperation(value = "根据课程id进行查询大纲列表",notes = "根据课程id进行查询大纲列表",httpMethod = "GET")
    @GetMapping(value = "/getChapterVideo/{courseId}")
    public R getChapterVideo(@ApiParam(name = "courseId",value = "课程ID",required = true)
                             @PathVariable("courseId") String courseId) {

        List<ChapterVo> chapterVoList = eduChapterService.getChapterVideo(courseId);

        return R.ok().data("allChapterVideo",chapterVoList);
    }


    /**
     * 添加章节
     * @return
     */
    @ApiOperation(value = "添加章节",notes = "添加章节",httpMethod = "POST")
    @PostMapping(value = "/addChapter")
    public R addChapter(@ApiParam(name = "eduChapter",value = "课程章节对象",required = true)
                        @RequestBody EduChapter eduChapter) {

        eduChapterService.save(eduChapter);

        return R.ok();
    }


    /**
     * 根据id查询章节信息
     * @param chapterId
     * @return
     */
    @ApiOperation(value = "根据id查询章节信息",notes = "根据id查询章节信息",httpMethod = "GET")
    @GetMapping(value = "/getChapterInfo/{chapterId}")
    public R getChapterInfo(@ApiParam(name = "chapterId",value = "章节信息id",required = true)
                            @PathVariable("chapterId") String chapterId) {
        EduChapter eduChapter = eduChapterService.getById(chapterId);
        return R.ok().data("chapter",eduChapter);
    }

    /**
     * 修改章节
     * @return
     */
    @ApiOperation(value = "修改章节",notes = "修改章节",httpMethod = "PUT")
    @PutMapping(value = "/updateChapter")
    public R updateChapter(@ApiParam(name = "eduChapter",value = "课程章节对象",required = true)
                               @RequestBody EduChapter eduChapter) {

        eduChapterService.updateById(eduChapter);

        return R.ok();
    }


    /**
     * 删除章节信息
     * @param chapterId
     * @return
     */
    @ApiOperation(value = "删除章节信息",notes = "删除章节信息",httpMethod = "DELETE")
    @DeleteMapping(value = "/deleteChapter/{chapterId}")
    public R deleteChapter(@ApiParam(name = "chapterId",value = "章节信息id",required = true)
                               @PathVariable("chapterId") String chapterId) {

        boolean flag = eduChapterService.deleteChapter(chapterId);

        if (flag) {
            return R.ok();
        }
        return R.error();
    }


}

