package com.xunqi.eduservice.controller;


import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课程简介 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */


@Api(value = "课程简介信息管理",tags = "课程简介信息管理")
@RestController
@RequestMapping("/eduservice/edudescription")
public class EduCourseDescriptionController {

}

