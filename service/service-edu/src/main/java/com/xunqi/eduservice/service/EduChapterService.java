package com.xunqi.eduservice.service;

import com.xunqi.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xunqi.eduservice.entity.chapter.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
public interface EduChapterService extends IService<EduChapter> {

    /**
     * 根据课程id，查询课程大纲列表
     * @param courseId
     * @return
     */
    List<ChapterVo> getChapterVideo(String courseId);

    /**
     * 删除章节信息
     * @param chapterId
     */
    boolean deleteChapter(String chapterId);
}
