package com.xunqi.eduservice.entity.chapter;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 章节小节
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-02 10:03
 **/

@Data
public class ChapterVo {

    private String id;

    private String title;

    //章节里面有多个视频小节
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<VideoVo> children = new ArrayList<>();

}
