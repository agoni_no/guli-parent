package com.xunqi.educms.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.commonutils.R;
import com.xunqi.educms.entity.CrmBanner;
import com.xunqi.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-06
 */

@Api(value = "后台Banner管理接口",tags = "后台Banner管理接口")
@RestController
@RequestMapping("/educms/banneradmin")
public class BannerAdminController {

    @Autowired
    private CrmBannerService bannerService;

    /**
     * 分页查询banner
     * @param page
     * @param limit
     * @return
     */
    @Cacheable(key = "'pageBanner'",value = "guli_banner")
    @ApiOperation(value = "获取Banner分页列表",notes = "获取Banner分页列表",httpMethod = "GET")
    @GetMapping(value = "/pageBanner/{page}/{limit}")
    public R pageBanner(@PathVariable("page") long page,@PathVariable("limit") long limit) {

        Page<CrmBanner> pageBanner = new Page<>(limit,page);

        IPage<CrmBanner> pageList = bannerService.page(pageBanner, null);

        Map<String,Object> map = new HashMap<>();
        map.put("item",pageList.getRecords());
        map.put("total",pageList.getTotal());

        return R.ok().data(map);
    }


    @ApiOperation(value = "根据id获取Banner信息",notes = "根据id获取Banner信息",httpMethod = "GET")
    @GetMapping(value = "/getBanner/{id}")
    public R getBanner(@PathVariable("id") String id) {

        CrmBanner crmBanner = bannerService.getById(id);

        return R.ok().data("banner",crmBanner);
    }

    /**
     * 添加
     * @param crmBanner
     * @return
     */
    @CachePut(key = "'addBanner'",value = "guli_banner")
    @ApiOperation(value = "新增Banner信息",notes = "新增Banner信息",httpMethod = "POST")
    @PostMapping(value = "/addBanner")
    public R addBanner(@RequestBody CrmBanner crmBanner) {

        bannerService.save(crmBanner);

        return R.ok();
    }

    /**
     * 修改
     * @param crmBanner
     * @return
     */
    @CacheEvict(key = "'updateBanner'",value = "guli_banner",allEntries = true)
    @ApiOperation(value = "修改Banner信息",notes = "修改Banner信息",httpMethod = "PUT")
    @PutMapping(value = "/updateBanner")
    public R updateBanner(@RequestBody CrmBanner crmBanner) {

        bannerService.updateById(crmBanner);

        return R.ok();
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @CacheEvict(key = "'remove'",value = "guli_banner",allEntries = true)
    @ApiOperation(value = "根据id删除Banner信息",notes = "根据id删除Banner信息",httpMethod = "DELETE")
    @DeleteMapping(value = "/remove/{id}")
    public R deleteBanner(@PathVariable("id") String id) {

        bannerService.removeById(id);

        return R.ok();
    }


}

