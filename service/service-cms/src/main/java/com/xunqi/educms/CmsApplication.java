package com.xunqi.educms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-06 16:10
 **/

@SpringBootApplication
@ComponentScan(basePackages = {"com.xunqi"})        //扫描指定位置
@MapperScan("com.xunqi.educms.mapper")
public class CmsApplication {

    public static void main(String[] args) {

        SpringApplication.run(CmsApplication.class,args);

    }

}
