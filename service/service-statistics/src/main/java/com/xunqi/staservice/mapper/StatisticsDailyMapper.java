package com.xunqi.staservice.mapper;

import com.xunqi.staservice.entity.StatisticsDaily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-13
 */
public interface StatisticsDailyMapper extends BaseMapper<StatisticsDaily> {

}
