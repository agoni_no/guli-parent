package com.xunqi.vod.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-04 18:38
 **/
public interface VodService {

    /**
     * 上传视频到阿里云
     * @param file
     * @return
     */
    String uploadAliFile(MultipartFile file) throws IOException;

    /**
     * 根据视频id删除上传到阿里云的视频
     * @param id
     */
    void removeAliyVideo(String id);

    /**
     * 删除多个视频
     * @param videoIdList
     */
    void removeMoreAliVideo(List<String> videoIdList);
}
