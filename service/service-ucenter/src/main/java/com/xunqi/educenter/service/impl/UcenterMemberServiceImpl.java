package com.xunqi.educenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xunqi.commonutils.JwtUtils;
import com.xunqi.commonutils.MD5;
import com.xunqi.educenter.entity.UcenterMember;
import com.xunqi.educenter.mapper.UcenterMemberMapper;
import com.xunqi.educenter.service.UcenterMemberService;
import com.xunqi.educenter.vo.RegisterVo;
import com.xunqi.educenter.vo.UcenterMemberVo;
import com.xunqi.servicebase.exception.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-08
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private final static String GULI_MSM = "guli_msm:";


    @Override
    public String login(UcenterMemberVo memberVo) {

        //获取登录手机号和密码
        String mobile = memberVo.getMobile();
        String password = memberVo.getPassword();

        //手机号和密码进行非空验证
        if (StringUtils.isEmpty(memberVo) || StringUtils.isEmpty(password)) {
            throw new GuliException(20001,"手机号或者密码不能为空");
        }

        //判断手机号是否正确
        UcenterMember memberInfo = this.baseMapper.selectOne(
                new QueryWrapper<UcenterMember>().eq("mobile", mobile));

        //判断查出来的数据是否为空
        if (memberInfo == null) {
            throw new GuliException(20001,"手机号不存在");
        }

        //把输入的密码进行加密和数据库中的密码进行匹配
        if (!MD5.encrypt(password).equals(memberInfo.getPassword())) {
            throw new GuliException(20001,"密码不正确");
        }

        //判断用户是否被禁用
        if (memberInfo.getIsDisabled()) {
            throw new GuliException(20001,"该账号已被禁用，无法登录");
        }

        //登录成功
        //生成token字符串，使用jwt工具类
        String jwtToken = JwtUtils.getJwtToken(memberInfo.getId(), memberInfo.getNickname());

        return jwtToken;
    }

    @Override
    public void register(RegisterVo registerVo) {

        //获取注册的数据
        String code = registerVo.getCode();     //验证码
        String phone = registerVo.getMobile();   //手机号
        String nickName = registerVo.getNickname(); //昵称
        String password = registerVo.getPassword(); //密码

        //判断验证码，获取Redis中验证码
        String redisCode = stringRedisTemplate.opsForValue().get(GULI_MSM + phone);
        if (StringUtils.isEmpty(redisCode)) {
            throw new GuliException(20001,"验证码已过期，请重新发送验证码");
        }
        if (!code.equals(redisCode.split("_")[0])) {
            throw new GuliException(20001,"验证码不正确");
        }

        //判断手机号是否重复，如果存在相同的手机号，则不进行注册
        Integer mobileCount = this.baseMapper.selectCount(
                new QueryWrapper<UcenterMember>().eq("mobile", phone));

        if (mobileCount > 0) {
            throw new GuliException(20001,"该手机号已注册过");
        }

        //数据添加到数据库中
        UcenterMember ucenterMember = new UcenterMember();
        ucenterMember.setMobile(phone);
        ucenterMember.setPassword(MD5.encrypt(password));
        ucenterMember.setNickname(nickName);
        ucenterMember.setIsDisabled(false);
        ucenterMember.setAvatar("https://guli-parent-edu.oss-cn-shenzhen.aliyuncs.com/2020-08-04/5e77036981a743638a05783dcb47b093file.png");

        int result = this.baseMapper.insert(ucenterMember);
        if (result > 0) {
            //如果注册成功，则需要删除Redis中保存的验证码
            stringRedisTemplate.delete(GULI_MSM + phone);
        }
    }

    @Override
    public UcenterMember getByOpenid(String openid) {

        UcenterMember member = this.baseMapper.selectOne(
                new QueryWrapper<UcenterMember>().eq("openid",openid));

        return member;
    }

    @Override
    public Integer countRegister(String day) {

        return this.baseMapper.countRegisterDay(day);
    }
}
