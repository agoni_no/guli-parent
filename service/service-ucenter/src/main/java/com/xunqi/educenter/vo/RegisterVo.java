package com.xunqi.educenter.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-08 14:20
 **/

@Data
@ApiModel(value="注册对象", description="注册对象")
public class RegisterVo {

    @ApiModelProperty(value = "昵称")
    @NotBlank(message = "昵称不允许为空")
    private String nickname;

    @ApiModelProperty(value = "手机号")
    @NotBlank(message = "手机号不允许为空")
    private String mobile;

    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不允许为空")
    private String password;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不允许为空")
    private String code;
}
