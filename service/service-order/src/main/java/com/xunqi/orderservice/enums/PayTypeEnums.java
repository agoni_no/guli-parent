package com.xunqi.orderservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 16:14
 **/

@AllArgsConstructor
@Getter
public enum PayTypeEnums {

    WXTYPE(1,"微信"),

    ALIYUN(2,"支付宝");

    private Integer code;

    private String desc;

}
